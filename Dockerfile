FROM node:buster

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
ENV DEBIAN_FRONTEND "noninteractive"
ENV HOME "/root"

ADD setup.sh $HOME
RUN bash $HOME/setup.sh

CMD git config --global init.defaultBranch 'master'
